import React from 'react'

function NotFound () {
    return (
        <div className="container">
            <div className="row vh-100">
                    <div className="col text-center display-6 align-self-center">
                        404 | Not Found
                    </div>
            </div>
        </div>
    )
}

export default NotFound;