import React from 'react'
import { NavLink } from 'react-router-dom';

function Login () {
    return (
        <div className="container">
            <div className="row vh-100 justify-content-center">
                    <div className="col-lg-4 col-md-6 col-sm-10 align-self-center">
                        <div className="mb-2">
                            <NavLink to="/">Back</NavLink>
                        </div>
                        <div className="card">
                            <div className="card-header">Login</div>
                            <div className="card-body">
                                <form>
                                    <div className="mb-4">
                                        <label htmlFor="email">Email</label>
                                        <input type="email" name="email" id="email" className="form-control"/>
                                    </div>
                                    <div className="mb-4">
                                        <label htmlFor="password">Password</label>
                                        <input type="password" name="password" id="password" className="form-control"/>
                                    </div>
                                    <button type="submit" className="btn btn-primary">Login</button>
                                </form>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    )
}

export default Login;